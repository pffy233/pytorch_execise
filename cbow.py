import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
CONTEXT_SIZE = 2  # 2 words to the left, 2 to the right
raw_text = """We are about to study the idea of a computational process.
Computational processes are abstract beings that inhabit computers.
As they evolve, processes manipulate other abstract things called data.
The evolution of a process is directed by a pattern of rules
called a program. People create programs to direct processes. In effect,
we conjure the spirits of the computer with our spells.""".split()

# By deriving a set from `raw_text`, we deduplicate the array
vocab = set(raw_text)
vocab_size = len(vocab)
embedding_dim = 10
word_to_ix = {word: i for i, word in enumerate(vocab)}
data = []
for i in range(2, len(raw_text) - 2):
    context = [raw_text[i - 2], raw_text[i - 1],
               raw_text[i + 1], raw_text[i + 2]]
    target = raw_text[i]
    data.append((context, target))
print(data[:5])


class CBOW(nn.Module):

    def __init__(self, vocab_size, embeding_dim):
        super(CBOW, slef).__init()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.linear = nn.Linear(embedding_dim, vocab_size)
    def forward(self, inputs):
        context = self.embedings(input)
        out = self.linear(torch.sum(context, dim = 0, keepdim = True))
        log_probs = F.log_softmax(out)
        return log_probs

# create your model and train.  here are some functions to help you make
# the data ready for use by your module


def make_context_vector(context, word_to_ix):
    idxs = [word_to_ix[w] for w in context]
    tensor = torch.LongTensor(idxs)
    return autograd.Variable(tensor)


make_context_vector(data[0][0], word_to_ix)  # example

loss = []
loss_func = nn.NLLLoss()
model = CBOW(vocab_size, embeding_size)
optimizer = optim.SGD(model.parameters(), lr = 0.01)
for epoch in range(10):
    total_loss = torch.Tensor([0])
    for context, target in data:

        model.zero_grad()
        prob = model(make_context_vector(context, word_to_ix))

        loss = loss_func(prob, autograd.Variable(torch.LongTensor(word_to_ix[target])))

        loss.backward()
        optim.step()
        total_loss += loss.data

print(loss)
